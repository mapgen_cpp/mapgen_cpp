#include "component.h"
#include <iostream>
#include <algorithm>
#include <chrono>

using namespace std;


Component::Component() {
    //~ start = {(uint)-1,(uint)-1};
    //~ end = {(uint)-1,(uint)-1};
}

Component::~Component() {
    points.clear();
    edges.clear();
}



void Component::addPoint( Point p ) {
    points.push_back( p );
}



bool Component::operator==( const Component &rhs ) {
    
    if( points.size() == rhs.points.size() ) {
        
        for( int i=0; i<points.size(); ++i ) {
            
            if( points[i].row != rhs.points[i].row &&
                points[i].col != rhs.points[i].col      )
                
                return false;
            
        }
        
        return true;
        
    }
    else
        return false;

}


void Component::findEdges() {
    for( uint i=0; i<points.size(); ++i ) {
        for( uint j=0; j<points.size(); ++j ) {
            
            if( points[i].row != points[j].row ||
                points[i].col != points[j].col      ) {
                
                int rowdiff = abs(int(points[i].row)-int(points[j].row));
                int coldiff = abs(int(points[i].col)-int(points[j].col));
                
                if( rowdiff < 2 && coldiff < 2 ) {
                    float w;
                    
                    if( rowdiff == 0 || coldiff == 0 )
                        w = 0.707f;
                    else
                        w = 1.0f;
                    
                    Edge e = {i,j,w};
                    
                    edges.push_back(e);
                }
            }
        }
    }
}



void Component::findStartEnd() {

    // allocate distances array
    float ** distances = new float * [points.size()];
    for( int i=0; i<points.size(); ++i )
        distances[i] = new float [points.size()];
        
    // set initial values
    for( int i=0; i<points.size(); ++i )
        for( int j=0; j<points.size(); ++j )
            distances[i][j] = MAX_F;
    
    for( int i=0; i<points.size(); ++i )
        distances[i][i] = 0.0f;
    
    for( int i=0; i<edges.size(); ++i )
        distances[edges[i].start][edges[i].end] = edges[i].weight;



    cout << "Starting Floyd-Warshall ..." << endl;
    cout << "Points: " << points.size() << endl;
    cout << "Edges: " << edges.size() << endl;

    std::chrono::time_point<std::chrono::high_resolution_clock> t1,t2;
	
    t1 = std::chrono::high_resolution_clock::now();
 
    // begin loop (Floyd-Warshall)
    for( int k=0; k<points.size(); ++k ) {
        for( int i=0; i<points.size(); ++i ) {
            for( int j=0; j<points.size(); ++j ) {
                distances[i][j] = min( distances[i][j], distances[i][k] + distances[k][j] );
            }
        }
    } // end Floyd-Warshall

    t2 = std::chrono::high_resolution_clock::now();
    float ret = std::chrono::duration_cast< std::chrono::duration<float> >(t2-t1).count();
    cout << "Time: " << ret << endl;
    cout << endl;
  
    // find start and end points
    float num = 0.0f;
    int r=-1,c=-1;
    for( int i=0; i<points.size(); i++ ) {
        for( int j=0; j<points.size(); j++ ) {
            if( distances[i][j] > num ) {
                num = distances[i][j];
                r = i;
                c = j;
            }
        }
    }
    
    end = points[c];
    start = points[r];
    
    // clean up distances array
    for( int i=0; i<points.size(); ++i )
        delete distances[i];
    delete distances;
}

