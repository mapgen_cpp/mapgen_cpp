#ifndef MAP_H
#define MAP_H

#include <cstdlib>
#include <list>
#include "component.h"

using namespace std;

class Map {

    public:
        // functions
        Map();
        Map(    const int w,
                const int h,
                const bool verbose=false,
                int cut=-1,
                int smoothing=-1    );
        ~Map();
        
        // data
        unsigned char ** data;
        int width;
        int height;
    
    
    private:
        // functions
        void _generateMap(  const int w,
                            const int h,
                            const bool verbose=false,
                            int cut=-1,
                            int smoothing=-1  );
        void _cutMap();
        void _smoothMap();
        int _getCount(int,int,int);
        void _deleteMap();
        void _printMap();
        void _printComponents();
        void _findComponents( bool verbose=0, float minsize = 0.025f );
        void _cullComponent( Component &c );
        
        // data
        list<Component> components;

};



#endif // MAP_H
