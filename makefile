OBJECTS = map.o component.o
SOURCES = $(OBJECTS:.o=.cpp)
#SRCDIR  = Sources 


LIBS	= 
#CFLAGS 	= -g -Wno-deprecated -std=c++11 -O3
CFLAGS = -g -Wno-deprecated -std=c++0x -O3
CC		= g++
EXECUTABLE = main.out



main: $(OBJECTS) driver.cpp
		g++ -o $(EXECUTABLE) $(CFLAGS) $(OBJECTS) driver.cpp $(LIBS)


map.o: map.h map.cpp
		g++ -c map.cpp $(CFLAGS)
component.o: component.h component.cpp
		g++ -c component.cpp $(CFLAGS)
#locals.o: locals.h locals.cpp
#		g++ -c locals.cpp $(CFLAGS)





clean:
		rm main.out *.o
