#include "map.h"
#include <cstdlib>
#include <queue>
#include <iostream>
#include <map>

using namespace std;


// default constructor
Map::Map()
{
    // set initial height/width values
    width = -1;
    height = -1;
    
    // some random map sizes, w=(35-85), h=(5-40)
    int w = 35 + rand()%50;
    int h = 12 + rand()%15;
    
    // use default cut and smoothing parameters
    _generateMap(w,h,false);

}

// parameterized constructor
Map::Map(   const int w,
            const int h,
            const bool verbose,
            int cut,
            int smoothing       )
{
    // use parameters to construct a map
    _generateMap(w,h,verbose,cut,smoothing);
}


// destructor
Map::~Map()
{
    
    _deleteMap();
    components.clear();

}




// internal map generation function
void Map::_generateMap( const int w,
                        const int h,
                        const bool verbose,
                        int cut,
                        int smoothing  )
{

    // delete current map
    _deleteMap();
    
    // set parameters
    width = w;
    height = h;
    
    if( cut == -1 )
        cut = 3 + rand()%3;
    if( smoothing == -1 )
        smoothing = 1 + rand()%3;

    // allocate memory for map and tmpMap
    data = new unsigned char * [height];
    for( int j=0; j<height; ++j ) {
        data[j] = new unsigned char [width];
    }
    
    
    // randomly generate map    
    for( int i=0; i<height; ++i )
        for( int j=0; j<width; ++j ) {
            if( rand()%100 < 45 )
                data[i][j] = '#';
            else
                data[i][j] = '.';
        }
    
    // print initial map    
    if( verbose ) {
        _printMap();
    }
    
    // cut
    for( int c=0; c<cut; ++c ) {
    
        // cut the map c times
        _cutMap();
        if( verbose )
            _printMap();
    
    }
    
    // smooth
    for( int s=0; s<smoothing; ++s ) {
    
        // smooth the map s times
        _smoothMap();
        if( verbose )
            _printMap();
    
    }
    
    if( verbose )
        cout << "Removing diagonal 2x2s ..." << endl;

    // remove diagonal 2x2s
    for( int i=0; i<height-1; ++i )
        for( int j=0; j<width-1; ++j ) {
            // different test depending on current square
            if( data[i][j] == '#' ) {
                
                if( data[i][j+1] == '.' && data[i+1][j] == '.' &&
                    data[i+1][j+1] == '#' ) {
                    
                    data[i][j] = '.';
                    data[i+1][j+1] = '.';
                
                }
                    
            }
            else {
                
                if( data[i][j+1] == '#' && data[i+1][j] == '#' &&
                    data[i+1][j+1] == '.'   ) {
                    
                    data[i][j+1] = '.';
                    data[i+1][j] = '.';
                
                }                
            }
        }
    
    // print final map
    if( verbose )
        _printMap();
        
    _findComponents( verbose );

    if( verbose ) {
        cout << "Components found: " << components.size() << endl;
    }
    
    // print culled map
    if( verbose )
        _printMap();
    
    
    // find start and end points for each component
    for( auto & c : components ) {
        c.findEdges();
        c.findStartEnd();
    }

    if( verbose ) {
        for( auto c : components ) {
            cout << "\ncomponent: " << c.points.size() << endl;
            cout << "start: " << c.start.row << ' ' << c.start.col << endl;
            cout << "end: " << c.end.row << ' ' << c.end.col << endl << endl;
        }
    }

    // print components map
    if( verbose )
        _printComponents();

}

void Map::_cutMap() {
    unsigned char ** newData;
    
    // allocate new data
    newData = new unsigned char * [height];
    for( int i=0; i<height; ++i )
        newData[i] = new unsigned char [width];
        
    // iterate over map
    for( int i=0; i<height; ++i )
        for( int j=0; j<width; ++j ) {
            
            
            if( _getCount(3,i,j) > 4 || _getCount(5,i,j) < 2 )
                newData[i][j] = '#';
            else
                newData[i][j] = '.';
                
        }
        
    // delete old map and assign new data
    _deleteMap();
    data = newData;
}


void Map::_smoothMap() {
    unsigned char ** newData;
    
    // allocate new data
    newData = new unsigned char * [height];
    for( int i=0; i<height; ++i )
        newData[i] = new unsigned char [width];
        
    // iterate over map
    for( int i=0; i<height; ++i )
        for( int j=0; j<width; ++j ) {
            
            
            if( _getCount(3,i,j) > 4 )
                newData[i][j] = '#';
            else
                newData[i][j] = '.';
                
        }
        
    // delete old map and assign new data
    _deleteMap();
    data = newData;
}


int Map::_getCount(int n, int u, int v) {
    
    // neighborhood shoud be odd
    if( n%2 == 0 ) ++n;
    
    int count = 0;
    // iterate over neighborhood
    for( int i=-n/2; i<n/2+1; ++i )
        for( int j=-n/2; j<n/2+1; ++j ) {
            
            // test for out-of-bounds
            bool test = (   v+j < 0 ||
                            u+i < 0 ||
                            u+i > height-1 ||
                            v+j > width-1       );
            
            if( test || data[u+i][v+j] == '#' )
                ++count;
            
        }
        
    return count;
}


// delete current map
void Map::_deleteMap() {

    // delete map data
    if( width != -1 ) {
    
        for( int i=0; i<height; ++i ) {
            delete data[i];
        }
    
        delete data;
    }
    
}

// print current map
void Map::_printMap() {

    cout << endl;
    for( int j=0; j<height; ++j ) {
        for( int i=0; i<width; ++i )
            cout << data[j][i];
        cout << endl;
    }
    
    cout << height << " rows " << width << " cols" << endl;
}


// print current map with component info
void Map::_printComponents() {

    cout << endl;
    for( uint i=0; i<height; ++i ) {
        for( uint j=0; j<width; ++j ) {
            
            bool test = false;
            for( auto c : components ) {
                if( c.start.row == i && c.start.col == j ) {
                    test = true;
                    cout << 'S';
                }
                else if( c.end.row == i && c.end.col == j ) {
                    test = true;
                    cout << 'E';
                }
            }
            
            if( !test )
                cout << data[i][j];
            
        
        }
        cout << endl;
    }
    
    cout << height << " rows " << width << " cols" << endl;
}


// find connected components
void Map::_findComponents( bool verbose, float minsize ) {
    queue<Point> q;
    bool ** marks;
    components.clear();
    
    // allocate new data
    marks = new bool * [height];
    for( uint i=0; i<height; ++i ) {
        marks[i] = new bool [width];
        for( uint j=0; j<width; ++j )
            marks[i][j] = 0;
    }
    
    // loop thru map data
    for( uint i=0; i<height; ++i ) {
        for( uint j=0; j<width; ++j ) {
            
            if( marks[i][j] != 1 ) {
            
                if( data[i][j] == '#' )
                    marks[i][j] = 1;
                else {
                    if( verbose )
                        cout << "Found component! ";
                    Component comp;
                    
                    q.push( {i,j} );
                    marks[i][j] = 1;
                    
                    while( !q.empty() ) {
                        
                        // process current point
                        Point p = q.front();
                        q.pop();
                        comp.points.push_back( p );
                        
                        // push neighbors
                        for( int u=-1; u<2; ++u ) {
                            for( int v=-1; v<2; ++v ) {
                                
                                bool test = p.row+u > height-1 ||
                                            p.col+v > width-1;
                                
                                if( !test ) {
                                    if( data[p.row+u][p.col+v] == '.' ) {
                                        
                                        // check mark
                                        if( marks[p.row+u][p.col+v] == 0 ) {
                                            marks[p.row+u][p.col+v] = 1;                                        
                                            q.push( {p.row+u,p.col+v} );
                                        }
                                    }       
                                }   
                            }
                        }
                    }
                    if( verbose ) {
                        cout << comp.points.size() << " points\t";
                        cout << comp.points.size()/float(width*height);
                    }
                    
                    
                    // only add component if it's large enough
                    if( float(comp.points.size())/float(width*height) < minsize ) {
                        _cullComponent( comp );
                        cout << " (too small)" << endl;
                    }
                    else {
                        components.push_back( comp );
                        cout << endl;
                    }
                }
            }
        }
    }
    
    // delete tmp data
    for( uint i=0; i<height; ++i ) {
        delete marks[i];
    }
    delete marks;
    
}

// cull connected component from the map
void Map::_cullComponent( Component &c ) {
    
    for( auto p : c.points ) {
        uint i = p.row;
        uint j = p.col;
        
        data[i][j] = '#';
    }
}


