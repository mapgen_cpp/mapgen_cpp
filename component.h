#ifndef COMPONENT_H
#define COMPONENT_H

#include <cstdlib>
#include <vector>
#include <limits>

#define MAX_F numeric_limits<float>::max()

using namespace std;

struct Point {
    uint row;
    uint col;
};


struct Edge {
    uint start;
    uint end;
    float weight;
};


class Component {
    
    public:
        // methods
        Component();
        ~Component();
        void addPoint(Point);
        void findEdges();
        void findStartEnd();
        
        bool operator==( const Component &rhs );
    
        // data
        vector<Point> points;
        vector<Edge> edges;
        
        Point start;
        Point end;
    
    private:
        // methods
        
        
        // data
    
};



#endif
